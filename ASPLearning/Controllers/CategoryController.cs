﻿using ASPLearning.DAO;
using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPLearning.Controllers
{
    public class CategoryController : Controller
    {

        public ActionResult Index(string id)
        {
            List<CourseDto> ListCourses = CourseDao.GetCourseByCategory(Convert.ToInt32(id));
            List<CategoryDto> ListCategories = CategoryDao.Gets();
            ViewBag.Title = "Category";
            ViewBag.ListCourses = ListCourses;
            ViewBag.ListCategories = ListCategories;
            return View();
        }
	}
}