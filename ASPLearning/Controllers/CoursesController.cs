﻿using ASPLearning.DAO;
using ASPLearning.Models;
using ASPLearning.Utils;
using ASPLearning.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
namespace ASPLearning.Controllers
{
   public class CoursesController : Controller
   {
      // GET: Courses
      public ActionResult Index(string id)
      {
         CourseDto course = CourseDao.getById(Convert.ToInt32(id));
         if (course == null)
            return RedirectToAction("Index", "Home");

         CourseViewModel courseView = new CourseViewModel(course);
         courseView.ListVideos = VideoDao.GetByCourseId(course.Id);
         if (Session["username"] != null)
         {
            string username = Convert.ToString(Session["username"]);
            UserDto user = UserDao.getByUserName(username);
            if (user.Type == 1 && !course.IsApproved)
            {
               return RedirectToAction("Index", "Home");
            }
            courseView.ListWatched = WatchDao.GetByUser(user.UserId, course.Id).Select(n => n.Video).ToList();
            PaymentDto payment = PaymentDao.GetByCourseAndUser(user.UserId, course.Id);
            if (payment != null)
               courseView.Applied = true;
            else
               courseView.Applied = false;
            //
            var teacher = UserDao.getById(course.TeacherId);
            int usertype = TypeUtils.Parse.ToInt(Session["usertype"]);
            if (usertype == 0 || (teacher != null && usertype == 2 && TypeUtils.Parse.ToString(Session["username"]).Equals(teacher.UserName)))
            {
               courseView.Applied = true;
            }
         }
         ViewBag.Title = course.Name;
         return View(courseView);
      }
      [HttpPost]
      public ActionResult ApplyCourse(string id)
      {
         int course_id = TypeUtils.Parse.ToInt(id);
         if (Session["username"] == null)
            return RedirectToAction("Index", "Login");
         CourseDto dto = CourseDao.getByCourseId(course_id);
         CourseManagerViewModel model = new CourseManagerViewModel()
         {
            Id = dto.Id,
            Name = dto.Name,
            Price = dto.Fee == 0 ? "Free" : "$" + dto.Fee.ToString()
         };
         model.Teacher = UserDao.getNameById(dto.TeacherId);
         model.Category = CategoryDao.GetNameById(dto.CategoryId);
         return View(model);
      }
      [HttpPost]
      public ActionResult Apply(string id)
      {
         int course_id = TypeUtils.Parse.ToInt(id);
         if (Session["username"] == null)
            return RedirectToAction("Index", "Login");
         //
         string username = Convert.ToString(Session["username"]);
         UserDto user = UserDao.getByUserName(username);
         PaymentDto payment = new PaymentDto();
         payment.User = user.UserId;
         payment.Course = course_id;
         payment.Type = Request.Form["exampleRadios"];
         payment.Amount = CourseDao.getPriceById(course_id);
         PaymentDao.Insert(payment);
         //
         return Redirect("/Courses?id=" + id); ;
      }
      public ActionResult Video(string id)
      {
         int VideoID = TypeUtils.Parse.ToInt(id);
         VideoDto video = VideoDao.GetById(VideoID);
         if (Session["username"] == null || video == null)
            return RedirectToAction("Index", "Home");
         
         VideoViewModel videoView = new VideoViewModel(video);
         videoView.Course = CourseDao.getById(video.CourseId);
         string username = Convert.ToString(Session["username"]);
         videoView.Teacher = UserDao.getByUserName(username);

         int UserId = TypeUtils.Parse.ToInt(Session["user_id"]);
         WatchDto watched = WatchDao.GetByVideoId(UserId, VideoID);
         videoView.IsWatched = (watched == null) ? false : true;
         //
         UserDto teacher = null;
         if (videoView.Course != null)
            teacher = UserDao.getById(videoView.Course.TeacherId);
         int usertype = TypeUtils.Parse.ToInt(Session["usertype"]);
         if (usertype == 0 || (teacher != null && usertype == 2 && TypeUtils.Parse.ToString(Session["username"]).Equals(teacher.UserName)))
         {
            videoView.IsWatched = true;
         }

         ViewBag.Title = video.Name;
         return View(videoView);
      }

      public ActionResult Register()
      {
         if (Session["username"] == null)
            return RedirectToAction("Index", "Home");
         UserDto dto = UserDao.getByUserName((string)Session["username"]); //Register (đang build)
         return View(dto);
      }
      [HttpPost]
      public ActionResult Complete()
      {
         int id = TypeUtils.Parse.ToInt(Request.Form["video_id"]);
         VideoDto video = VideoDao.GetById(id);
         if (Session["username"] == null || video == null)
            return RedirectToAction("Index", "Home");
         
         WatchDto dto = new WatchDto();
         dto.Course = video.CourseId;
         dto.Video = video.Id;
         dto.User = TypeUtils.Parse.ToInt(Session["user_id"]);
         WatchDao.Insert(dto);
         return Redirect("/Courses?id=" + video.CourseId); ;
      }
   }
}