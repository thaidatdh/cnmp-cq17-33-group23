﻿using ASPLearning.DAO;
using ASPLearning.Models;
using ASPLearning.Utils;
using ASPLearning.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
namespace ASPLearning.Controllers
{
   public class DashboardController : Controller
   {
      // GET: Dashboard
      public ActionResult Index()
      {
         if (Session["username"] == null)
            return RedirectToAction("Index", "Login");
         UserDto user = UserDao.getByUserName((string)Session["username"]);
         if (user == null)
         {
            return Redirect("/");
         }
         DashboardViewModel model = new DashboardViewModel(user);
         model.ListMyCourses = CourseDao.GetListCourseViewByLearner(user.UserId);
         if (Convert.ToInt32(Session["usertype"]) == 2)
         {
            model.ListCourses = CourseDao.GetListViewByOwner(user.UserId);
            foreach (var course in model.ListCourses)
            {
               course.ListVideos = VideoDao.GetByCourseId(course.Id);
            }
            //
            CourseManagerViewModel emptyCourse = new CourseManagerViewModel()
            {
               Id = 0,
               Name = "Not assigned",
               Category = "Not assigned",
               ListVideos = VideoDao.GetByCourseId(user.UserId, 0)
            };
            if (emptyCourse.ListVideos.Count != 0)
               model.ListCourses.Add(emptyCourse);
         }
         return View(model);
      }
      [HttpPost]
      public ActionResult ChangePassword(UserDto changedDto)
      {
         if (String.IsNullOrEmpty(TypeUtils.Parse.ToString(Session["username"])))
            return RedirectToAction("Index", "Login");
         var old_pwd = TypeUtils.Parse.ToString(Request.Form["old_password"]);
         UserDto dto = UserDao.getByUserName((string)Session["username"]);
         if (!ConversionUtils.Users.Password(old_pwd).Equals(dto.Password))
         {
            Session["error_changed_password"] = "Old password incorrect! Please enter the right password.";
            return View("Index");
         }
         else
         {
            Session["error_changed_password"] = "";
         }
         var pwd = ConversionUtils.Users.Password(changedDto.Password);
         UserDao.updatePassword(dto.UserId, pwd);
         return RedirectToAction("Index", "Dashboard");
      }
      public ActionResult Information(UserDto changedDto)
      {
         if (String.IsNullOrEmpty(Session["username"].ToString()))
            return RedirectToAction("Index", "Login");
         UserDto dto = UserDao.getByUserName(Session["username"].ToString());
         if (!String.IsNullOrEmpty(changedDto.UserName))
         {
            dto.LastName = changedDto.LastName;
            dto.FirstName = changedDto.FirstName;
            dto.Email = changedDto.Email;
            dto.DOB = changedDto.DOB;
            UserDao.update(dto);
         }
         return View(dto);
      }
      [HttpPost]
      public ActionResult Delete(string id)
      {
         CourseDao.UpdateDeleted(TypeUtils.Parse.ToInt(id), true);
         return Redirect("/Dashboard#courses");
      }
      [HttpPost]
      public ActionResult DeleteVideo(string id)
      {
         int a = VideoDao.Delete(TypeUtils.Parse.ToInt(id));
         return Redirect("/Dashboard#videos");
      }
      public ActionResult Review()
      {
          if (Session["username"] == null)
              return RedirectToAction("Index", "Home");
          ReviewDto dto = ReviewDao.getByUserName((string)Session["username"]); //Review (đang build)
          return View(dto);
      }
   }
}