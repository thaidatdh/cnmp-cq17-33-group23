﻿using ASPLearning.DAO;
using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPLearning.Controllers
{
   public class HomeController : Controller
   {
      public ActionResult Index()
      {
         List<CourseDto> ListCourses = CourseDao.GetNew();
         List<CourseDto> ListCourses2 = CourseDao.GetPopular();
         List<CategoryDto> ListCategories = CategoryDao.Gets();
         ViewBag.Title = "Home";
         ViewBag.ListCourses = ListCourses;
         ViewBag.ListCourses2 = ListCourses2;
         ViewBag.ListCategories = ListCategories;
         return View();
      }

      public ActionResult About()
      {
         ViewBag.Message = "Your application description page.";

         return View();
      }

      public ActionResult Contact()
      {
         ViewBag.Message = "Your contact page.";

         return View();
      }
   }
}