﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPLearning.DAO;
using ASPLearning.Models;
using ASPLearning.Utils;

namespace ASPLearning.Controllers
{
   public class LoginController : Controller
   {
      // GET: Login
      public ActionResult Index()
      {
         Session.Remove("error_login");
         Session.Remove("error_signup");
         if (Session["username"] != null )
         {
            return RedirectToAction("Index", "Home");
         }
         return View();
      }
      [HttpPost]
      public ActionResult AuthorizeMain()
      {
         Session.Remove("error_signup");
         var password = TypeUtils.Parse.ToString(Request.Form["pwd"]);
         var username = TypeUtils.Parse.ToString(Request.Form["username"]);
         var userData = UserDao.getByUserName(username);
         if (String.IsNullOrEmpty(username) || userData == null || !userData.Password.Equals(ConversionUtils.Users.Password(password)))
         {
            return RedirectToAction("Index", "Home");
         }
         else
         {
            Session.Remove("error_login");
            Session["username"] = userData.UserName;
            Session["usertype"] = userData.Type;
            Session["user_id"] = userData.UserId;
            return RedirectToAction("Index", "Home");
         }
      }
      [HttpPost]
      public ActionResult Authorize(UserDto userModel)
      {
         Session.Remove("error_signup");
         var pwd = ConversionUtils.Users.Password(userModel.Password);
         var userData = UserDao.getByUserName(userModel.UserName);
         if (userData == null || !userData.Password.Equals(pwd) || String.IsNullOrEmpty(userModel.UserName))
         {
            Session["error_login"] = "Sign in: Username or Password invalid!";
            return View("Index", userModel);
         }
         else
         {
            Session.Remove("error_login");
            Session["username"] = userData.UserName;
            Session["usertype"] = userData.Type;
            Session["user_id"] = userData.UserId;
            return RedirectToAction("Index", "Home");
         }
      }
      [HttpPost]
      public ActionResult SignUp(UserDto dto)
      {
         Session.Remove("error_login");
         var userData = UserDao.getByUserName(dto.UserName);
         if (String.IsNullOrEmpty(dto.UserName) || String.IsNullOrEmpty(dto.Password))
         {
            Session["error_signup"] = "Sign up: Username or Password invalid!";
            return View("index", dto);
         }
         else if (userData != null)
         {
            Session["error_signup"] = "Sign up: " + dto.UserName + " already exists!";
            return View("index", dto);
         }
         else
         {
            var retype = TypeUtils.Parse.ToString(Request.Form["retype_password"]);
            if (!retype.Equals(dto.Password))
            {
               Session["error_signup"] = "Sign up: Retype password incorrect! Please enter same password.";
               return View("index", dto);
            }
            var pwd = ConversionUtils.Users.Password(dto.Password);
            int type = TypeUtils.Parse.ToInt(Request.Form["usertype"]);
            dto.Password = pwd;
            if (type == 1)
               dto.IsActive = true;
            else
               dto.IsActive = false;
            dto.CreatedDate = DateTime.Now;
            dto.Type = type;
            var result = UserDao.insert(dto);
            if (result != -1)
            {
               Session.Remove("error_signup");
               Session["username"] = dto.UserName;
               Session["usertype"] = dto.Type;
               Session["user_id"] = userData.UserId;
               return RedirectToAction("Index", "Home");
            }
            else
            {
               Session["error_signup"] = "Sign up: Fail to sign up. Please try again!";
               return View("index", dto);
            }
         }
      }
      public ActionResult Logout()
      {
         Session.Abandon();
         return RedirectToAction("Index", "Home");
      }
   }
}