﻿using ASPLearning.DAO;
using ASPLearning.Models;
using ASPLearning.Utils;
using ASPLearning.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPLearning.Controllers
{
   public class ManagementController : Controller
   {
      // GET: Management
      public ActionResult Index()
      {
         if (Session["username"] == null)
            return RedirectToAction("Index","Login");
         if (TypeUtils.Parse.ToInt(Session["usertype"]) != 0)
            return RedirectToAction("Index", "Home");
         //
         ManagementViewModel model = new ManagementViewModel();
         model.ListTeachers = UserDao.GetAllTeacher().OrderByDescending(n => n.IsActive).ToList();
         model.ListCourses = CourseDao.GetListViewManagement();
         foreach (var course in model.ListCourses)
         {
            var list = VideoDao.GetByCourseId(course.Id).Select(n => new VideoViewModel(n)).ToList();
            model.ListVideos.AddRange(list);
         }
         var unAssignedVideos = VideoDao.GetByCourseId(0);
         if (unAssignedVideos.Count != 0)
         {
            foreach (var video in unAssignedVideos)
               model.ListVideos.Add(new VideoViewModel(video));
         }
         foreach (var video in model.ListVideos)
         {
            var course = model.ListCourses.FirstOrDefault(n => n.Id == video.CourseId);
            if (course == null)
               video.Course = new CourseDto() { Name = "Unknown" };
            else
               video.Course = new CourseDto() { Name = course.Name, Id = course.Id, IsApproved = course.IsApproved};
            var teacher = model.ListTeachers.FirstOrDefault(n => n.UserId == video.OwnerId);
            if (teacher == null)
               video.Teacher = new UserDto() { FirstName = "Unknown", LastName = "" };
            else
               video.Teacher = teacher;
         }
         return View(model);
      }
      [HttpPost]
      public ActionResult UnApprove(string id)
      {
         CourseDao.UpdateApprove(TypeUtils.Parse.ToInt(id), false);
         return Redirect("/Management#courses");
      }
      [HttpPost]
      public ActionResult Approve(string id)
      {
         CourseDao.UpdateApprove(TypeUtils.Parse.ToInt(id), true);
         return Redirect("/Management#courses");
      }
      [HttpPost]
      public ActionResult UnConfirm(string id)
      {
         UserDao.updateStatus(TypeUtils.Parse.ToInt(id), false);
         return Redirect("/Management#users");
      }
      [HttpPost]
      public ActionResult Confirm(string id)
      {
         UserDao.updateStatus(TypeUtils.Parse.ToInt(id), true);
         return Redirect("/Management#users");
      }
   }
}