﻿using ASPLearning.DAO;
using ASPLearning.Models;
using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPLearning.Controllers
{
   public class PostCourseController : Controller
   {
      //
      // GET: /PostCourse/
      public ActionResult Index()
      {
         if (Session["username"] == null || TypeUtils.Parse.ToInt(Session["usertype"]) != 2)
            return Redirect("/");

         List<CategoryDto> ListCategories = CategoryDao.Gets();
         ViewBag.ListCategories = ListCategories;
         //setCatViewBag();
         return View();
      }
      public ActionResult Video()
      {
         if (Session["username"] == null || TypeUtils.Parse.ToInt(Session["usertype"]) != 2)
            return Redirect("/");
         int teacher = TypeUtils.Parse.ToInt(Session["user_id"]);
         List<CourseDto> listCourse = CourseDao.GetByTeacher(teacher);
         ViewBag.listCourse = listCourse;
         return View();
      }
      [HttpPost]
      public ActionResult PostVideo(VideoDto dto)
      {
         dto.CreatedDate = DateTime.Now;
         dto.UpdatedDate = DateTime.Now;
         dto.OwnerId = Convert.ToInt32(Session["user_id"]);
         int result = VideoDao.insert(dto);
         if (result != -1)
         {
            return Redirect("/Dashboard#courses");
         }
         else
         {
            Session["error_postcourse"] = "Post Course: Fail to post your course. Please check your some infomations and try again!";
            return View("Video", dto);
         }
      }
      [HttpPost]
      public ActionResult Post(CourseDto dto)
      {
         dto.CreatedTime = DateTime.Now;
         dto.IsApproved = false;
         dto.TeacherId = Convert.ToInt32(Session["user_id"]);
         int result = 0;
         if (!CourseDao.isExists(dto))
            result = CourseDao.insert(dto);
         if (result != -1)
         {
            return Redirect("/Dashboard#courses");
         }
         else
         {
            Session["error_postcourse"] = "Post Course: Fail to post your course. Please check your some infomations and try again!";
            return View("index", dto);
         }
      }

      public void setCatViewBag(long? selectedID = null)
      {
         ViewBag.CategoryID = new SelectList(CategoryDao.Gets(), "Id", "name", selectedID);
      }
   }
}