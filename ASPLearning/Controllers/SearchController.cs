﻿using ASPLearning.DAO;
using ASPLearning.Utils;
using ASPLearning.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPLearning.Controllers
{
   public class SearchController : Controller
   {
      // GET: Search
      public ActionResult Index()
      {
         SearchModel model = new SearchModel();
         int SearchType = TypeUtils.Parse.ToInt(Request.Form["search_type"]);
         string keyword = TypeUtils.Parse.ToString(Request.Form["search_value"]).Trim().ToLower();
         model.Keyword = keyword.ToUpper();
         if (SearchType == 0)
            return View();
         model.SearchType = SearchType;
         List<int> list_id = new List<int>();
         switch (SearchType)
         {
            case -1:
               model.ListCourse = CourseDao.GetsNotDeleted().FindAll(n => n.Name.ToLower().Contains(keyword) && n.IsApproved == true);
               model.ListCategory = CategoryDao.Gets().FindAll(n => n.Name.ToLower().Contains(keyword));
               model.ListTeacher = UserDao.GetAllTeacher().FindAll(n => n.FirstName.ToLower().Contains(keyword) || n.LastName.ToLower().Contains(keyword));
               break;
            case 1:
               model.ListCourse = CourseDao.GetsNotDeleted().FindAll(n => n.Name.ToLower().Contains(keyword) && n.IsApproved==true);
               break;
            case 2:
               model.ListCategory = CategoryDao.Gets().FindAll(n => n.Name.ToLower().Contains(keyword));
               list_id = model.ListCategory.Select(n => n.Id).ToList();
               model.ListCourse = CourseDao.GetsNotDeleted().FindAll(n => list_id.Contains(n.CategoryId) && n.IsApproved == true);
               break;
            case 3:
               model.ListTeacher = UserDao.GetAllTeacher().FindAll(n => n.FirstName.ToLower().Contains(keyword) || n.LastName.ToLower().Contains(keyword));
               list_id = model.ListTeacher.Select(n => n.UserId).ToList();
               model.ListCourse = CourseDao.GetsNotDeleted().FindAll(n => list_id.Contains(n.TeacherId) && n.IsApproved == true);
               break;
            default:
               model.ErrorMessage = "Search type not match!";
               break;
         }
         return View(model);
      }
   }
}