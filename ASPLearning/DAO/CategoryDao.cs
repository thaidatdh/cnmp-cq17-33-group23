﻿using ASPLearning.Models;
using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.DAO
{
   public class CategoryDao
   {
      public static List<CategoryDto> Gets()
      {
         return Database.SelectQuery("select * from Category").Rows.Cast<DataRow>().Select(n => new CategoryDto(n)).ToList();
      }
        public static IEnumerable <CategoryDto> GetAllCate()
      {
          List<CategoryDto> allCate = Gets();
          return allCate;
      }
      public static string GetNameById(int id)
      {
         var list = Database.SelectQuery("select name from Category where id=" + id).Rows.Cast<DataRow>().Select(n => Convert.ToString(n["name"])).ToList();
         if (list == null || list.Count == 0)
            return "NaN";
         else
            return list.First();
      }
   }
}