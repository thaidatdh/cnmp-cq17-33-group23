﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ASPLearning.Models;
using ASPLearning.Utils;
namespace ASPLearning.DAO
{
   public class CommentDao
   {
      public static List<CommentDto> Gets()
      {
         return Database.SelectQuery("select * from Comment").Rows.Cast<DataRow>().Select(n => new CommentDto(n)).ToList();
      }
   }
}