﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ASPLearning.Models;
using ASPLearning.Utils;
using ASPLearning.ViewModels;
namespace ASPLearning.DAO
{
   public class CourseDao
   {
      public static List<CourseDto> Gets()
      {
         return Database.SelectQuery("select * from Course").Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList();
      }
      public static List<CourseDto> GetsNotDeleted()
      {
         return Database.SelectQuery("select * from Course where is_deleted='False'").Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList();
      }
      public static List<CourseDto> GetPopular()
      {
          return Database.SelectQuery("select top 5 c.*, count(DISTINCT w.user_id) as num_students from Course c left join Watched w on c.Id = w.course where c.is_deleted='False' group by c.id, c.name,c.teacher_id,c.category_id, c.fee, c.created_time,"+
             " c.description,c.intro_video_link,c.is_approved,c.is_deleted " + " order by num_students desc").Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList().FindAll(n => n.IsApproved == true);
      }
      public static List<CourseDto> GetNew()
      {
          return Database.SelectQuery("select top 9 * from Course where is_deleted='False' order by Id desc ").Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList().FindAll(n => n.IsApproved == true);
      }
      public static List<CourseDto> GetCourseByCategory(int cate)
      {
          return Database.SelectQuery("select * from Course where is_deleted='False' and category_id=" + cate).Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList().FindAll(n => n.IsApproved == true);
      }
      public static List<CourseDto> GetByTeacher(int teacher_id)
      {
         return Database.SelectQuery("select * from Course where teacher_id=" + teacher_id).Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList();
      }
      public static bool isExists(CourseDto dto)
      {
         string checkQuery = "Select * from course where name='" + dto.Name + "' and teacher_id=" + dto.TeacherId + " and category_id=" + dto.CategoryId
                              + " and fee=" + dto.Fee + " and intro_video_link='" + dto.IntroVideoLink.Replace("'", "''") + "'";
         int rows = Database.SelectQuery(checkQuery).Rows.Count;
         if (rows == 0)
            return false;
         return true;
      }
      public static List<CourseManagerViewModel> GetListViewManagement()
      {
         List<CourseManagerViewModel> result = new List<CourseManagerViewModel>();
         List<CourseDto> list = CourseDao.GetsNotDeleted().OrderBy(n => n.IsApproved).ThenBy(n => n.CreatedTime).ToList();
         int count = 0;
         foreach (var dto in list)
         {
            CourseManagerViewModel model = new CourseManagerViewModel();
            model.Order = ++count;
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Price = (dto.Fee == 0) ? "Free" : "$" + dto.Fee.ToString();
            model.Category = CategoryDao.GetNameById(dto.CategoryId);
            model.CreatedTime = dto.CreatedTime.ToString("MM/dd/yyyy");
            model.IsApproved = dto.IsApproved;
            var teacher = UserDao.getById(dto.TeacherId);
            if (teacher != null)
               model.Teacher = teacher.FirstName + " " + teacher.LastName;
            else
               model.Teacher = "Unknown";
            result.Add(model);
         }
         return result;
      }
      public static List<CourseManagerViewModel> GetListViewByOwner(int TeacherId)
      {
         string sql = "select * from Course where teacher_id=" + TeacherId;
         List<CourseDto> list = Database.SelectQuery(sql).Rows.Cast<DataRow>().Select(n => new CourseDto(n)).OrderBy(n => n.CreatedTime).ToList().FindAll(n => n.IsDeleted == false);
         List<CourseManagerViewModel> result = new List<CourseManagerViewModel>();
         int count = 0;
         foreach(CourseDto dto in list)
         {
            CourseManagerViewModel model = new CourseManagerViewModel();
            model.Order = ++count;
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Price = (dto.Fee == 0) ? "Free" : "$" + dto.Fee.ToString();
            model.Category = CategoryDao.GetNameById(dto.CategoryId);
            model.CreatedTime = dto.CreatedTime.ToString("MM/dd/yyyy");
            model.IsApproved = dto.IsApproved;
            result.Add(model);
         }
         return result;
      }
      public static List<MyCourseViewModel> GetListCourseViewByLearner(int LearnerId)
      {
         string sql = "select c.* from Payment p, Course c where p.user_id=" + LearnerId + " and p.course=c.id";
         List<CourseDto> listCourses = Database.SelectQuery(sql).Rows.Cast<DataRow>().Select(n => new CourseDto(n)).ToList();
         List<MyCourseViewModel> result = new List<MyCourseViewModel>();
         int count = 0;
         foreach(CourseDto course in listCourses)
         {
            MyCourseViewModel model = new MyCourseViewModel();
            model.Order = ++count;
            model.Id = course.Id;
            model.Name = course.Name;
            model.Price = course.Fee.ToString();
            //
            var teacher = UserDao.getById(course.TeacherId);
            model.Teacher = (teacher == null) ? "" : (teacher.FirstName + " " + teacher.LastName);
            //
            double numOfVideo = (double)VideoDao.CountByCourse(course.Id);
            double numOfWatched = (double)WatchDao.CountByCourse(LearnerId, course.Id);
            model.CompletedPercent = numOfWatched.Equals(0) ? "0.0%" : (numOfWatched / numOfVideo).ToString("P1");
            result.Add(model);
         }
         return result;
      }
      public static CourseDto getByUserName(string uid)
      {
         if (String.IsNullOrEmpty(uid))
            return null;
         var list = Database.SelectQuery("select * from Course_Users where username='" + uid.Replace("'", "''") + "'");
         if (list == null || list.Rows.Count == 0)
            return null;
         return new CourseDto(list.Rows[0]);
      }
      public static CourseDto getById(int id)
      {
         var list = Database.SelectQuery("select * from Course where Id=" + id);
         if (list == null || list.Rows.Count == 0)
            return null;
         return new CourseDto(list.Rows[0]);
      }
      public static int getPriceById(int id)
      {
         var list = Database.SelectQuery("select * from Course where Id=" + id);
         if (list == null || list.Rows.Count == 0)
            return 0;
         var dto = new CourseDto(list.Rows[0]);
         return dto.Fee;
      }
      public static CourseDto getByCourseName(string uid)
      {
         if (String.IsNullOrEmpty(uid))
            return null;
         var list = Database.SelectQuery("select * from Course where coursename='" + uid.Replace("'", "''") + "'");
         if (list == null || list.Rows.Count == 0)
            return null;
         return new CourseDto(list.Rows[0]);
      }
      public static CourseDto getByCourseId(int id)
      {
         var list = Database.SelectQuery("select * from Course where Id=" + id);
         if (list == null || list.Rows.Count == 0)
            return null;
         return new CourseDto(list.Rows[0]);
      }
      public static int insert(CourseDto dto)
      {
         string sql = ConversionUtils.Models.getInsertCourseSql(dto);
         return Database.ExecuteQuery(sql);
      }
      public static int update(CourseDto dto)
      {
         string sql = ConversionUtils.Models.getUpdateCourseSql(dto);
         return Database.ExecuteQuery(sql);
      }
      public static int UpdateApprove(int id, bool isApproved)
      {
         string sql = "update Course set is_approved='" + isApproved.ToString() + "' where Id=" + id;
         return Database.ExecuteQuery(sql);
      }
      public static int UpdateDeleted(int id, bool isDeleted)
      {
         string sql = "update Course set is_deleted='" + isDeleted.ToString() + "' where Id=" + id;
         return Database.ExecuteQuery(sql);
      }
      public static string getTeacherbyCourseId(int id)
      {
          string sql = "select u.first_name,u.last_name  from Course c join Users u on c.teacher_id = u.user_id where c.Id = " + id;
          var list =  Database.SelectQuery(sql);
          string s1 = list.Rows[0]["first_name"].ToString();
          string s2 = list.Rows[0]["last_name"].ToString();
          return String.Concat(s1," ",s2);
           
      }
   }
}