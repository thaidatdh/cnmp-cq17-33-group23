﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ASPLearning.Models;
using ASPLearning.Utils;
namespace ASPLearning.DAO
{
   public class PaymentDao
   {
      public static List<PaymentDto> Gets()
      {
         return Database.SelectQuery("select * from Payment").Rows.Cast<DataRow>().Select(n => new PaymentDto(n)).ToList();
      }
      public static PaymentDto GetByCourseAndUser(int user_id, int course_id)
      {
         var list = Database.SelectQuery("select * from Payment where user_id=" + user_id + " and course=" + course_id).Rows.Cast<DataRow>().Select(n => new PaymentDto(n)).ToList();
         if (list.Count == 0)
            return null;
         else
            return list[0];
      }
      public static int Insert(PaymentDto dto)
      {
         string sql = ConversionUtils.Models.getInsertPaymentSql(dto);
         return Database.ExecuteQuery(sql);
      }
   }
}