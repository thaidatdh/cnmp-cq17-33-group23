﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ASPLearning.Models;
using ASPLearning.Utils;
namespace ASPLearning.DAO
{
   public class ReviewDao
   {
      public static List<ReviewDto> Gets()
      {
         return Database.SelectQuery("select * from Review").Rows.Cast<DataRow>().Select(n => new ReviewDto(n)).ToList();
      }
      public static ReviewDto getByUserName(string uid)
      {
          if (String.IsNullOrEmpty(uid))
              return null;
          var list = Database.SelectQuery("select * from Review where username='" + uid.Replace("'", "''") + "'");
          if (list == null || list.Rows.Count == 0)
              return null;
          return new ReviewDto(list.Rows[0]);
      }
      public static ReviewDto getById(int id)
      {
          var list = Database.SelectQuery("select * from Review where Id=" + id);
          if (list == null || list.Rows.Count == 0)
              return null;
          return new ReviewDto(list.Rows[0]);
      }
      public static int insert(ReviewDto dto)
      {
          string sql = ConversionUtils.Models.getInsertReviewSql(dto);
          return Database.ExecuteQuery(sql);
      }
      public static int update(ReviewDto dto)
      {
          string sql = ConversionUtils.Models.getUpdateReviewSql(dto);
          return Database.ExecuteQuery(sql);
      }
   }
    
}