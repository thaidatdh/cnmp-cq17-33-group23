﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASPLearning.Utils;
using System.Data;

namespace ASPLearning.DAO
{
   public class UserDao
   {
      public static List<UserDto> Gets()
      {
         return Database.SelectQuery("select * from Users").Rows.Cast<DataRow>().Select(n => new UserDto(n)).ToList();
      }
      public static List<UserDto> GetAllTeacher()
      {
         return Database.SelectQuery("select * from Users where type=2").Rows.Cast<DataRow>().Select(n => new UserDto(n)).ToList();
      }
      public static UserDto getByUserName(string uid)
      {
         if (String.IsNullOrEmpty(uid))
            return null;
         var list = Database.SelectQuery("select * from Users where username='" + uid.Replace("'", "''") + "'");
         if (list == null || list.Rows.Count == 0)
            return null;
         return new UserDto(list.Rows[0]);
      }
      public static UserDto getById(int id)
      {
         var list = Database.SelectQuery("select * from Users where user_id="+id);
         if (list == null || list.Rows.Count == 0)
            return null;
         return new UserDto(list.Rows[0]);
      }
      public static string getNameById(int id)
      {
         var list = Database.SelectQuery("select * from Users where user_id=" + id);
         if (list == null || list.Rows.Count == 0)
            return "";
         var dto = new UserDto(list.Rows[0]);
         return dto.FirstName + " " + dto.LastName;
      }
      public static int insert(UserDto dto)
      {
         string sql = ConversionUtils.Models.getInsertUserSql(dto);
         return Database.ExecuteQuery(sql);
      }
      public static int update(UserDto dto)
      {
         string sql = ConversionUtils.Models.getUpdateUserSql(dto);
         return Database.ExecuteQuery(sql);
      }
      public static int updatePassword(int user_id, string password)
      {
         string sql = "update Users set password='" + password + "' where user_id=" + user_id;
         return Database.ExecuteQuery(sql);
      }
      public static int updateStatus(int user_id, bool is_active)
      {
         string sql = "update Users set is_active='" + is_active.ToString() + "' where user_id=" + user_id;
         return Database.ExecuteQuery(sql);
      }
   }
}