﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ASPLearning.Models;
using ASPLearning.Utils;
using ASPLearning.ViewModels;

namespace ASPLearning.DAO
{
   public class VideoDao
   {
      public static List<VideoDto> Gets()
      {
         return Database.SelectQuery("select * from Videos").Rows.Cast<DataRow>().Select(n => new VideoDto(n)).ToList();
      }
      public static List<VideoDto> GetByCourseId(int course_id)
      {
         return Database.SelectQuery("select * from Videos where course=" + course_id).Rows.Cast<DataRow>().Select(n => new VideoDto(n)).OrderBy(n => n.OrderId).ToList();
      }
      public static List<VideoDto> GetByCourseId(int owner_id, int course_id)
      {
         return Database.SelectQuery("select * from Videos where course=" + course_id + " and owner_id="+owner_id).Rows.Cast<DataRow>().Select(n => new VideoDto(n)).OrderBy(n => n.OrderId).ToList();
      }
      public static VideoDto GetById(int id)
      {
         var list = Database.SelectQuery("select * from Videos where id=" + id).Rows.Cast<DataRow>().Select(n => new VideoDto(n)).ToList();
         if (list.Count == 0)
            return null;
         else
            return list.First();
      }
      public static int CountByCourse(int course_id)
      {
         string sql = "select count(*) as counter from Videos where course=" + course_id;
         var list = Database.SelectQuery(sql).Rows.Cast<DataRow>().Select(n => TypeUtils.Parse.ToInt(n["counter"])).ToList();
         if (list.Count == 0)
            return 0;
         else
            return list.First();
      }
      public static int Delete(int id)
      {
         string sql = "delete from Videos where id=" + id;
         return Database.ExecuteQuery(sql);
      }
      public static int insert(VideoDto dto)
      {
         string sql = "insert into Videos(name, course, created_date, updated_date, description, video_link, order_id, owner_id) values ";
         var order = Database.SelectQuery("select max(order_id) from Videos where course=" + dto.CourseId).Rows.Cast<DataRow>().FirstOrDefault();
         int order_id = 1;
         if (order!=null)
         {
            order_id = TypeUtils.Parse.ToInt(order.ItemArray.FirstOrDefault()) + 1;
         }
         sql += "(" + ConversionUtils.Models.getFormatedObj(dto.Name) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(dto.CourseId) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(dto.CreatedDate) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(dto.UpdatedDate) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(dto.Description) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(dto.VideoLink) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(order_id) + ","
            + "(" + ConversionUtils.Models.getFormatedObj(dto.OwnerId) + ")";
         return Database.ExecuteQuery(sql);
      }
   }
}