﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ASPLearning.Models;
using ASPLearning.Utils;
namespace ASPLearning.DAO
{
   public class WatchDao
   {
      public static List<WatchDto> Gets()
      {
         return Database.SelectQuery("select * from Watched").Rows.Cast<DataRow>().Select(n => new WatchDto(n)).ToList();
      }
      public static List<WatchDto> GetByUser(int UserId, int CourseId)
      {
         string sql = "select * from Watched where user_id=" + UserId + " and course=" + CourseId;
         return Database.SelectQuery(sql).Rows.Cast<DataRow>().Select(n => new WatchDto(n)).ToList();
      }
      public static WatchDto GetByVideoId(int UserId, int VideoId)
      {
         string sql = "select * from Watched where user_id=" + UserId + " and video=" + VideoId;
         return Database.SelectQuery(sql).Rows.Cast<DataRow>().Select(n => new WatchDto(n)).ToList().FirstOrDefault();
      }
      public static int CountByCourse(int UserId, int CourseId)
      {
         string sql = "select count(*) as counter from Watched where user_id=" + UserId + " and course=" + CourseId;
         var list = Database.SelectQuery(sql).Rows.Cast<DataRow>().Select(n => TypeUtils.Parse.ToInt(n["counter"])).ToList();
         if (list.Count == 0)
            return 0;
         else
            return list.First();
      }
      public static int Insert(WatchDto dto)
      {
         string sql = ConversionUtils.Models.getInsertWatchedSql(dto);
         return Database.ExecuteQuery(sql);
      }
   }
}