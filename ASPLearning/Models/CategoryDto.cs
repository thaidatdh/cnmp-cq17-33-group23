﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class CategoryDto
   {
      public int Id { get; set; }
      public string Name { get; set; }
      public CategoryDto() { }
      public CategoryDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         Name = dr["name"].ToString();
      }
   }
}