﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class CommentDto
   {
      public int Id { get; set; }
      public int CreatedBy { get; set; }
      public DateTime CreatedTime { get; set; }
      public string Comment { get; set; }
      public CommentDto() 
      {
         CreatedTime = DateTime.Now;
      }
      public CommentDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         CreatedBy = TypeUtils.Parse.ToInt(dr["created_by"]);
         CreatedTime = DateTime.Parse(dr["created_time"].ToString());
         Comment = dr["comment"].ToString();
      }
   }
}