﻿using ASPLearning.DAO;
using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class CourseDto
   {
      public int Id { get; set; }
      public string Name { get; set; }
      public int TeacherId { get; set; }
      public int CategoryId { get; set; }
      public int Fee { get; set; }
      public DateTime CreatedTime { get; set; }
      public string Description { get; set; }
      public string IntroVideoLink { get; set; }
      public bool IsApproved { get; set; }
      public bool IsDeleted { get; set; }
      public CourseDto() 
      {
         CreatedTime = DateTime.Now;
         IsApproved = false;
         IsDeleted = false;
      }
      public CourseDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         Name = dr["name"].ToString();
         TeacherId = TypeUtils.Parse.ToInt(dr["teacher_id"]);
         CategoryId = TypeUtils.Parse.ToInt(dr["category_id"]);
         Fee = TypeUtils.Parse.ToInt(dr["fee"]);
         CreatedTime = DateTime.Parse(dr["created_time"].ToString());
         Description = dr["description"].ToString();
         IntroVideoLink = dr["intro_video_link"].ToString();
         IsApproved = Convert.ToBoolean(dr["is_approved"].ToString());
         IsDeleted = Convert.ToBoolean(dr["is_deleted"].ToString());
      }
       public string getTeacher(){
           return CourseDao.getTeacherbyCourseId(Id);
       }
   }
}