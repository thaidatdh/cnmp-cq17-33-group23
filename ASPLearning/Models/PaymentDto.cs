﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class PaymentDto
   {
      public int Id { get; set; }
      public int Course { get; set; }
      public int User { get; set; }
      public string Type { get; set; }
      public int Amount { get; set; }
      public DateTime CreatedTime { get; set; }
      public PaymentDto() {
         CreatedTime = DateTime.Now;
      }
      public PaymentDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         Course = TypeUtils.Parse.ToInt(dr["course"]);
         User = TypeUtils.Parse.ToInt(dr["user_id"]);
         Type = dr["type"].ToString();
         CreatedTime = DateTime.Parse(dr["created_time"].ToString());
         Amount = TypeUtils.Parse.ToInt(dr["amount"]);
      }
   }
}