﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class ReviewDto
   {
      public int Id { get; set; }
      public int Course { get; set; }
      public int CreatedBy { get; set; }
      public DateTime CreatedTime { get; set; }
      public int Rating { get; set; }
      public string Comment { get; set; }
      public ReviewDto() 
      {
         CreatedTime = DateTime.Now;
      }
      public ReviewDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         Course = TypeUtils.Parse.ToInt(dr["course"]);
         CreatedBy = TypeUtils.Parse.ToInt(dr["created_by"]);
         CreatedTime = DateTime.Parse(dr["created_time"].ToString());
         Rating = TypeUtils.Parse.ToInt(dr["Id"]);
         Comment = dr["comment"].ToString();
      }
   }
}