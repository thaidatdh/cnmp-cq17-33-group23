﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class UserDto
   {
      public int UserId { get; set; }
      [DisplayName("First name")]
      public string FirstName { get; set; }
      [DisplayName("Last name")]
      public string LastName { get; set; }
      //Type: 1 is Student, 2 is Teacher
      public int Type { get; set; }
      [DisplayName("Username")]
      [Required(ErrorMessage = "Username is required.")]
      public string UserName { get; set; }
      [DisplayName("Password")]
      [Required(ErrorMessage = "Password is required.")]
      public string Password { get; set; }
      [DisplayName("Email")]
      [EmailAddress(ErrorMessage = "Invalid email address.")]
      [Required(ErrorMessage = "Email is required.")]
      public string Email { get; set; }
      public bool IsActive { get; set; }
      public DateTime CreatedDate { get; set; }
      [DisplayName("Date of Birth")]
      [Required(ErrorMessage = "Date of Birth is required.")]
      [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
      public string DOB { get; set; }
      public string PhotoLink { get; set; }
      public UserDto() 
      {
         CreatedDate = DateTime.Now;
         PhotoLink = CONST.USER.DEFAULT_PHOTO_LINK;
      }
      public UserDto(DataRow dr)
      {
         PhotoLink = CONST.USER.DEFAULT_PHOTO_LINK;
         UserId = TypeUtils.Parse.ToInt(dr["user_id"]);
         FirstName = dr["first_name"].ToString();
         LastName = dr["last_name"].ToString();
         Type = TypeUtils.Parse.ToInt(dr["type"]);
         UserName = dr["username"].ToString();
         Password = dr["password"].ToString();
         Email = dr["email"].ToString();
         IsActive = TypeUtils.Parse.ToBoolean(dr["is_active"].ToString());
         CreatedDate = DateTime.Parse(dr["created_date"].ToString());
         if (!String.IsNullOrEmpty(dr["dob"].ToString()))
            DOB = DateTime.Parse(dr["dob"].ToString()).ToString("yyyy-MM-dd");
         else
            DOB = DateTime.MinValue.ToString("yyyy-MM-dd");
      }
   }
}