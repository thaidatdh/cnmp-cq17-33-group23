﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class VideoDto
   {
      public int Id { get; set; }
      public string Name { get; set; }
      public int CourseId { get; set; }
      public DateTime CreatedDate { get; set; }
      public DateTime UpdatedDate { get; set; }
      public string Description { get; set; }
      public string VideoLink { get; set; }
      public int OrderId { get; set; }
      public int OwnerId { get; set; }
      public VideoDto()
      {
         CreatedDate = DateTime.Now;
         UpdatedDate = CreatedDate;
      }
      public VideoDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         Name = dr["name"].ToString();
         CourseId = TypeUtils.Parse.ToInt(dr["course"]);
         CreatedDate = DateTime.Parse(dr["created_date"].ToString());
         string updateDate = dr["updated_date"].ToString();
         if (!String.IsNullOrEmpty(updateDate))
            UpdatedDate = DateTime.Parse(updateDate);
         else
            UpdatedDate = CreatedDate;
         Description = dr["description"].ToString();
         VideoLink = dr["video_link"].ToString();
         OrderId = TypeUtils.Parse.ToInt(dr["order_id"]);
         OwnerId = TypeUtils.Parse.ToInt(dr["owner_id"]);
      }
   }
}