﻿using ASPLearning.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ASPLearning.Models
{
   public class WatchDto
   {
      public int Id { get; set; }
      public int User { get; set; }
      public int Video { get; set; }
      public int Course { get; set; }
      public WatchDto() { }
      public WatchDto(DataRow dr)
      {
         Id = TypeUtils.Parse.ToInt(dr["Id"]);
         User = TypeUtils.Parse.ToInt(dr["user_id"]);
         Video = TypeUtils.Parse.ToInt(dr["video"]);
         Course = TypeUtils.Parse.ToInt(dr["course"]);
      }
   }
}