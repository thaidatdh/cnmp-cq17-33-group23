﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.Utils
{
   public class CONST
   {
      public static string DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
      public static class DATABASE
      {
         public static readonly string PWD = "shipe";
         public static readonly string UID = "thaidatdh";
         public static readonly string DBN = "thaidatdh_asplearning";
         public static readonly string SERVER = @"sql.freeasphost.net\MSSQL2016";
         public static readonly string[] TABLE_USERS = { "first_name", "last_name", "type", "username", "password", "email", "is_active", "created_date", "dob" };
         public static readonly string[] TABLE_CATEGORY = { "name" };
         public static readonly string[] TABLE_COMMENT = { "created_by", "created_time", "comment" };
         public static readonly string[] TABLE_COURSE = { "name", "teacher_id", "category_id", "fee", "created_time", "description", "intro_video_link", "is_approved" };
         public static readonly string[] TABLE_PAYMENT = { "course", "user_id", "type", "created_time", "amount" };
         public static readonly string[] TABLE_REVIEW = { "course", "created_by", "created_time", "rating", "comment" };
         public static readonly string[] TABLE_VIDEOS = { "name", "course", "created_date", "updated_date", "description", "video" };
         public static readonly string[] TABLE_WATCHED = { "user_id", "video", "course" };
      }
      public static class USER
      {
         public static string ACTIVE = "TRUE";
         public static string INACTICE = "FALSE";
         public static string DEFAULT_PHOTO_LINK = "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png";
      }
   }
}