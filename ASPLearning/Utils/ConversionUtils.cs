﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.Utils
{
   public class ConversionUtils
   {
      public static class Users
      {
         public static string Password(string pwd)
         {
            if (String.IsNullOrEmpty(pwd))
               return null;
            return CryptoUtils.encryptSHA256(CryptoUtils.Base64Encode(pwd));
         }
      }
      public static class Models
      {
         public static string getInsertWatchedSql(WatchDto dto)
         {
            object[] values = { dto.User, dto.Video, dto.Course };
            string sql_Fields = "";
            string sql_Values = "";
            bool isStarted = false;
            for (int i = 0; i < CONST.DATABASE.TABLE_WATCHED.Length; i++)
            {
               if (values[i] == null)
                  continue;
               sql_Values += isStarted ? ("," + getFormatedObj(values[i])) : getFormatedObj(values[i]);
               sql_Fields += isStarted ? ("," + CONST.DATABASE.TABLE_WATCHED[i]) : CONST.DATABASE.TABLE_WATCHED[i];
               isStarted = true;
            }
            return "insert into Watched (" + sql_Fields + ") values (" + sql_Values + ");";
         }
         public static string getInsertPaymentSql(PaymentDto dto)
         {
            object[] values = { dto.Course, dto.User, dto.Type, dto.CreatedTime, dto.Amount };
            string sql_Fields = "";
            string sql_Values = "";
            bool isStarted = false;
            for (int i = 0; i < CONST.DATABASE.TABLE_PAYMENT.Length; i++)
            {
               if (values[i] == null)
                  continue;
               sql_Values += isStarted ? ("," + getFormatedObj(values[i])) : getFormatedObj(values[i]);
               sql_Fields += isStarted ? ("," + CONST.DATABASE.TABLE_PAYMENT[i]) : CONST.DATABASE.TABLE_PAYMENT[i];
               isStarted = true;
            }
            return "insert into Payment (" + sql_Fields + ") values (" + sql_Values + ");";
         }
         public static string getInsertUserSql(UserDto dto)
         {
            object[] values = { dto.FirstName, dto.LastName, dto.Type, dto.UserName, dto.Password, dto.Email, dto.IsActive, dto.CreatedDate, dto.DOB };
            string sql_Fields = "";
            string sql_Values = "";
            bool isStarted = false;
            for (int i = 0; i < CONST.DATABASE.TABLE_USERS.Length; i++)
            {
               if (values[i] == null)
                  continue;
               sql_Values += isStarted ? ("," + getFormatedObj(values[i])) : getFormatedObj(values[i]);
               sql_Fields += isStarted ? ("," + CONST.DATABASE.TABLE_USERS[i]) : CONST.DATABASE.TABLE_USERS[i];
               isStarted = true;
            }
            return "insert into Users (" + sql_Fields + ") values (" + sql_Values + ");";
         }
         public static string getUpdateUserSql(UserDto dto)
         {
            object[] values = { dto.FirstName, dto.LastName, dto.Email,dto.DOB };
            string[] field = { "first_name", "last_name", "email", "dob" };
            string sql = "";
            bool isStarted = false;
            for (int i = 0; i < values.Length; i++)
            {
               if (values[i] == null)
                  continue;
               string temp = field[i] + "=" + getFormatedObj(values[i]);
               sql += isStarted ? ("," + temp) : temp;
               isStarted = true;
            }
            return "update Users set " + sql + " where user_id="+dto.UserId+";";
         }
         public static string getFormatedObj(object obj)
         {
            string result = "";
            Type type = obj.GetType();
            if (type.Equals(typeof(int)))
               result = ((int)obj).ToString();
            else if (type.Equals(typeof(string)))
               result = "'" + ((string)obj).ToString().Replace("'","''") + "'";
            else if (type.Equals(typeof(bool)))
               result = "'" + ((bool)obj).ToString().ToUpper() + "'";
            else if (type.Equals(typeof(DateTime)))
            {
               if (!((DateTime)obj).Equals(DateTime.MinValue))
                  result = "'" + ((DateTime)obj).ToString(CONST.DEFAULT_DATETIME_FORMAT) + "'";
               else
                  return "NULL";
            }
            return result;
         }

         public static string getInsertReviewSql(ReviewDto dto)
         {
             object[] values = { dto.Id, dto.Course, dto.CreatedBy,dto.CreatedTime,dto.Rating,dto.Comment };
             string sql_Fields = "";
             string sql_Values = "";
             bool isStarted = false;
             for (int i = 0; i < CONST.DATABASE.TABLE_REVIEW.Length; i++)
             {
                 if (values[i] == null)
                     continue;
                 sql_Values += isStarted ? ("," + getFormatedObj(values[i])) : getFormatedObj(values[i]);
                 sql_Fields += isStarted ? ("," + CONST.DATABASE.TABLE_REVIEW[i]) : CONST.DATABASE.TABLE_REVIEW[i];
                 isStarted = true;
             }
             return "insert into Review (" + sql_Fields + ") values (" + sql_Values + ");";
         }
         public static string getUpdateReviewSql(ReviewDto dto)
         {
             object[] values = { dto.Id, dto.Course, dto.CreatedBy, dto.CreatedTime, dto.Rating, dto.Comment };
             string[] field = { "ID", "Course", "CreatedBy", "CreatedTime", "Rating", "Comment" };
             string sql = "";
             bool isStarted = false;
             for (int i = 0; i < values.Length; i++)
             {
                 if (values[i] == null)
                     continue;
                 string temp = field[i] + "=" + getFormatedObj(values[i]);
                 sql += isStarted ? ("," + temp) : temp;
                 isStarted = true;
             }
             return "update Review set " + sql + " where review_id=" + dto.Id + ";";
         }

         public static string getInsertCourseSql(CourseDto dto)
         {
             object[] values = { dto.Name, dto.TeacherId, dto.CategoryId, dto.Fee, dto.CreatedTime, dto.Description, dto.IntroVideoLink, dto.IsApproved };
             string sql_Fields = "";
             string sql_Values = "";
             bool isStarted = false;
             for (int i = 0; i < CONST.DATABASE.TABLE_COURSE.Length; i++)
             {
                 if (values[i] == null)
                     continue;
                 sql_Values += isStarted ? ("," + getFormatedObj(values[i])) : getFormatedObj(values[i]);
                 sql_Fields += isStarted ? ("," + CONST.DATABASE.TABLE_COURSE[i]) : CONST.DATABASE.TABLE_COURSE[i];
                 isStarted = true;
             }
             return "insert into Course (" + sql_Fields + ") values (" + sql_Values + ");";
         }
         public static string getUpdateCourseSql(CourseDto dto)
         {
             object[] values = { dto.Name, dto.TeacherId, dto.CategoryId, dto.Fee, dto.CreatedTime, dto.Description, dto.IsApproved };
             string[] field = { "Name", "TeacherId", "CategoryId", "Fee", "CreatedTime" , "Description", "is_approved"};
             string sql = "";
             bool isStarted = false;
             for (int i = 0; i < values.Length; i++)
             {
                 if (values[i] == null)
                     continue;
                 string temp = field[i] + "=" + getFormatedObj(values[i]);
                 sql += isStarted ? ("," + temp) : temp;
                 isStarted = true;
             }
             return "update Course set " + sql + " where ID=" + dto.Id + ";";
         }
        }
   }
}