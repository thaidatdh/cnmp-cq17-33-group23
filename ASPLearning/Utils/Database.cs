﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace ASPLearning.Utils
{
   public class Database
   {
      private static string m_connectionString = @"Server=" + CONST.DATABASE.SERVER + ";Database=" + CONST.DATABASE.DBN + ";uid=" + CONST.DATABASE.UID + ";pwd=" + CONST.DATABASE.PWD + ";";
      private static SqlConnection connection;
      private static SqlCommand m_command;
      public static SqlConnection CreateConnection()
      {
         if (connection == null || !ConnectionState.Open.Equals(connection.State))
         {
            connection = new SqlConnection(m_connectionString);
            connection.Open();
         }

         return connection;
      }

      public static SqlCommand CreateCommand(string a_sqlCommandText)
      {
         m_command = null;
         while (m_command == null)
         {
            try
            {
               m_command = new SqlCommand(a_sqlCommandText, (SqlConnection)CreateConnection());
            }
            catch (Exception ex)
            {
               m_command = null;
            }
         }
         return m_command;
      }

      public static DataTable SelectQuery(string sql)
      {
         SqlCommand odbCommand = Database.CreateCommand(sql);
         SqlDataAdapter dataAdapter = new SqlDataAdapter(odbCommand);
         DataTable dt = new DataTable("tableName");
         dataAdapter.Fill(dt);
         dataAdapter.Dispose();
         return dt;
      }

      public static int ExecuteQuery(string sql)
      {
         int result = -1;
         try
         {
            var command = CreateCommand(sql);
            result = command.ExecuteNonQuery();
         }
         catch (Exception ex) { }
         return result;
      }
   }
}