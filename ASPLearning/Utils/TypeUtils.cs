﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.Utils
{
   public class TypeUtils
   {
      public static class Parse
      {
         public static int ToInt(object obj)
         {
            int result;
            try
            {
               result = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
               result = 0;
            }
            return result;
         }
         public static string ToString(object obj)
         {
            string result;
            try
            {
               result = Convert.ToString(obj);
            }
            catch (Exception ex)
            {
               result = "";
            }
            return result;
         }
         public static bool ToBoolean(object obj)
         {
            bool result;
            try
            {
               result = Convert.ToBoolean(obj);
            }
            catch (Exception ex)
            {
               result = true;
            }
            return result;
         }
      }
   }
}