﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class CourseManagerViewModel
   {
      public int Order { get; set; }
      public int Id { get; set; }
      public string Name { get; set; }
      public string Price { get; set; }
      public string Category { get; set; }
      public string CreatedTime { get; set; }
      public string Teacher { get; set; }
      public bool IsApproved { get; set; }
      public List<VideoDto> ListVideos { get; set; }
      public CourseManagerViewModel()
      {
         ListVideos = new List<VideoDto>();
      }
   }
}