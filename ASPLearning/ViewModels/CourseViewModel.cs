﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class CourseViewModel : CourseDto
   {
      public List<VideoDto> ListVideos { get; set; }
      public List<int> ListWatched { get; set; }
      public bool Applied { get; set; }
      public CourseViewModel() : base()
      {
         ListVideos = new List<VideoDto>();
         ListWatched = new List<int>();
         Applied = false;
      }
      public CourseViewModel(CourseDto courseDto)
      {
         foreach (PropertyInfo prop in courseDto.GetType().GetProperties())
         {
            PropertyInfo prop2 = courseDto.GetType().GetProperty(prop.Name);
            prop2.SetValue(this, prop.GetValue(courseDto, null), null);
         }
         ListWatched = new List<int>();
         ListVideos = new List<VideoDto>();
         Applied = false;
      }
   }
}