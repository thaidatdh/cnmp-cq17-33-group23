﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class DashboardViewModel : UserDto
   {
      public List<MyCourseViewModel> ListMyCourses { get; set; }
      public List<CourseManagerViewModel> ListCourses { get; set; }
      public DashboardViewModel(UserDto user)
      {
         foreach (PropertyInfo prop in user.GetType().GetProperties())
         {
            PropertyInfo prop2 = user.GetType().GetProperty(prop.Name);
            prop2.SetValue(this, prop.GetValue(user, null), null);
         }
         ListMyCourses = new List<MyCourseViewModel>();
         ListCourses = new List<CourseManagerViewModel>();
      }
   }
}