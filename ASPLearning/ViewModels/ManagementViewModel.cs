﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class ManagementViewModel
   {
      public List<UserDto> ListTeachers { get; set; }
      public List<CourseManagerViewModel> ListCourses { get; set; }
      public List<VideoViewModel> ListVideos { get; set; }
      public ManagementViewModel()
      {
         ListTeachers = new List<UserDto>();
         ListCourses = new List<CourseManagerViewModel>();
         ListVideos = new List<VideoViewModel>();
      }
   }
}