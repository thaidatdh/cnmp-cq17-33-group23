﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class MyCourseViewModel
   {
      public int Order { get; set; }
      public int Id { get; set; }
      public string Name { get; set; }
      public string Teacher { get; set; }
      public string Price { get; set; }
      public string CompletedPercent { get; set; }
   }
}