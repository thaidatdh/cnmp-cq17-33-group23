﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class SearchModel
   {
      public string ErrorMessage { get; set; }
      public int SearchType { get; set; }
      public List<CourseDto> ListCourse { get; set; }
      public List<CategoryDto> ListCategory { get; set; }
      public List<UserDto> ListTeacher { get; set; }
      public string Keyword { get; set; }
      public SearchModel()
      {
         Keyword = "";
         ListCourse = new List<CourseDto>();
         ListCategory = new List<CategoryDto>();
         ListTeacher = new List<UserDto>();
      }
   }
}