﻿using ASPLearning.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ASPLearning.ViewModels
{
   public class VideoViewModel : VideoDto
   {
      public CourseDto Course { get; set; }
      public UserDto Teacher { get; set; }
      public bool IsWatched { get; set; }
      public VideoViewModel() : base()
      {
         Course = new CourseDto();
         Teacher = new UserDto();
         IsWatched = false;
      }
      public VideoViewModel(DataRow dr) : base(dr)
      {
         Course = new CourseDto();
         Teacher = new UserDto();
         IsWatched = false;
      }
      public VideoViewModel(VideoDto video)
      {
         foreach (PropertyInfo prop in video.GetType().GetProperties())
         {
            PropertyInfo prop2 = video.GetType().GetProperty(prop.Name);
            prop2.SetValue(this, prop.GetValue(video, null), null);
         }
         Course = new CourseDto();
         Teacher = new UserDto();
         IsWatched = false;
      }
   }
}